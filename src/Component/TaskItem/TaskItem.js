import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {CardActions, withStyles} from "@material-ui/core";
import styles from './styles'
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';

class TaskItem extends Component {
    render() {
        const {classes, taskValue, status} = this.props;
        return (
            <Card className={classes.root}>
                <CardContent className={classes.content}>
                    <Grid container justify="space-between">
                        <Grid item className="md-8">
                            <Typography variant="h5" component="h2">
                                {taskValue.name}
                            </Typography>
                            <Typography component="h5">
                                {taskValue.description}
                            </Typography>

                        </Grid>
                        <Grid item className="md-4">
                            {status.label}
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions className={classes.cardAction}>
                    <Fab color="primary" size="small" aria-label="edit">
                        <Icon fontSize="small">
                            edit_icon
                        </Icon>
                        {/*<EditIcon size="small"/>*/}
                    </Fab>
                    <Fab color="secondary" size="small" aria-label="delete">
                        <Icon fontSize="small">
                            delete_icon
                        </Icon>
                        {/*<DeleteIcon  />*/}
                    </Fab>
                </CardActions>
            </Card>
        );
    }
}

export default withStyles(styles)(TaskItem);