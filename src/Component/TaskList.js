import React, {Component} from 'react';
import {withStyles} from "@material-ui/core";
import styles from '../TaskBoard/styles'
import TaskItem from "./TaskItem/TaskItem";

class TaskList extends Component {
    render() {
        const {taskValue, status} = this.props
        return <TaskItem taskValue={taskValue} status={status}/>
    }
}

export default withStyles(styles)(TaskList);