import React, {Component} from 'react';
import './App.css';
import {ThemeProvider, withStyles} from '@material-ui/core/styles';
import styles from './style/Style.js'
import TaskBoard from "./TaskBoard";
import theme from "./commons/Theme/theme"

// const useStyles = makeStyles(theme => ({
//   root: {
//     '& > *': {
//       margin: theme.spacing(1),
//     },
//   },
// }));

class App extends Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <TaskBoard/>
            </ThemeProvider>
        );
    }
}

export default withStyles(styles)(App);
