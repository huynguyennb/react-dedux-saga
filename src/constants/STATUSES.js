const STATUSES = [
    {
        label:"READY",
        value:0
    },{
        label:"COMPLETED",
        value:2
    },{
        label:"IN PROGRESS",
        value:1
    }
];
export default STATUSES;