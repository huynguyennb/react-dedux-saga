import {createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
    color: {
        primary: "#E64A19",
        secondary: "#009688",
        error: "#D32F2F"
    },
    typography: {
        fontFamily: "Roboto"
    },
    shape: {
        backgroundColor: "red",
        color: "white",
        borderColor: "#ccc",
        padding: 20,
        margin: 10,
        borderRadius: 4
    }
});
export default theme