import React, {Component} from 'react';
import styles from './styles'
import {withStyles} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import STATUSES from '../constants/STATUSES'
import Box from '@material-ui/core/Box';
import TaskList from "../Component/TaskList";
import TaskForm from "../Component/TaskForm/index";

const listTask = [
    {
        name: "Play football",
        description: "with friends",
        status: 0
    },
    {
        name: "Play Goals",
        description: "with friends",
        status: 1
    },
    {
        name: "Play girl",
        description: "with girl friend",
        status: 2
    }
];


class TaskBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    handleClose = () => {
        this.setState({
            open: false
        });
    };
    renderForm = () => {
        const {open} = this.state;
        let xhtml = null;
        xhtml = (
            <TaskForm open={open} onClose={this.handleClose}/>
        );
        return xhtml
    };

    renderBoard() {
        const {classes} = this.props;
        let xhtml = null;
        xhtml = (
            <Grid container spacing={2}>
                {
                    STATUSES.map((status, index) => {
                        const filterListTask = listTask.filter((task) => status.value === task.status);
                        return <Grid item md={4} sx={12} key={status.value}>
                            <Box mt={2} ml={2}>
                                <div className={classes.status}> {status.label}</div>
                            </Box>
                            <div className={classes.wraperListTask}>
                                {filterListTask.map((taskValue, indexTask) => {
                                    return <TaskList key={indexTask} filterListTask={filterListTask}
                                                     taskValue={taskValue}
                                                     status={status}/>
                                })}
                            </div>
                        </Grid>
                    })
                }
            </Grid>
        );
        return xhtml
    }

    openForm = () => {
        this.setState({
            open: true
        })
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.taskboard}>
                <Button variant="contained" color="primary" onClick={() => {
                    this.openForm()
                }}>
                    <AddIcon/>Thêm mới công việc
                </Button>
                {
                    this.renderBoard()
                }
                {
                    this.renderForm()
                }
            </div>
        );
    }
}

export default withStyles(styles)(TaskBoard);