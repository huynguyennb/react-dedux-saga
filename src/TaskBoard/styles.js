const styles = theme => ({
    taskboard: {
        // display: "flex",
        alignItems: "center"
    },
    shape: {
        // backgroundColor: "red",
        // color: "white",
        // borderColor: "#ccc",
        // padding: 20,
        // margin: 10,
        // borderRadius: 4
        backgroundColor: theme.shape.backgroundColor,
        color: theme.shape.color,
        borderColor: theme.shape.borderColor,
        padding: 20,
        margin: 10,
        borderRadius: theme.shape.borderRadius
    }
});
export default styles